package com.example.ensiklopediahewan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var btnquit : Button = findViewById(R.id.btnquit)
        btnquit.setOnClickListener {
            val alertdialog : AlertDialog = AlertDialog.Builder(this).create()
            alertdialog.setTitle("Keluar Aplikasi?")
            alertdialog.setMessage("Apakah anda ingin keluar dari Aplikasi?")

            alertdialog.setButton(AlertDialog.BUTTON_POSITIVE,"Ya"){
                dialog, which -> finish()
                dialog.dismiss()}

            alertdialog.setButton(AlertDialog.BUTTON_NEGATIVE,"Tidak"){
                dialog, which ->
                dialog.dismiss()
            }

            alertdialog.show()
        }



        btnherbivora.setOnClickListener{
            val intent = Intent(this, activity_herbivora::class.java)
            startActivity(intent)
        }

        btnkarnivora.setOnClickListener{
            val intent = Intent(this, activity_karnivora::class.java)
            startActivity(intent)
        }

        btnomnivora.setOnClickListener{
            val intent = Intent( this, activity_omnivora::class.java)
            startActivity(intent)
        }



        btnabout.setOnClickListener{
            val intent = Intent(this, activity_about::class.java)
            startActivity(intent)
        }

        btnpengertian.setOnClickListener{
            val intent = Intent(this, activity_penjelasan::class.java)
            startActivity(intent)
        }
    }
}