package com.example.ensiklopediahewan

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Karnivora(
    var name: String,
    var description: String,
    var photo: String
) : Parcelable