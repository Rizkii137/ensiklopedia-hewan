package com.example.ensiklopediahewan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_herbivora.*

class activity_herbivora : AppCompatActivity() {

    private val list = ArrayList<Herbivora>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_herbivora)

        rv_herbivora.setHasFixedSize(true)

        list.addAll(getListHerbivora())
        showRecyclerList()
    }

    fun getListHerbivora(): ArrayList<Herbivora> {
        val dataName = resources.getStringArray(R.array.data_name_herbivora)
        val dataDescription = resources.getStringArray(R.array.data_description_herbivora)
        val dataPhoto = resources.getStringArray(R.array.data_photo_herbivora)
        val listHerbivora = ArrayList<Herbivora>()
        for (position in dataName.indices) {
            val herbivora = Herbivora(
                dataName[position],
                dataDescription[position],
                dataPhoto[position]
            )
            listHerbivora.add(herbivora)
        }
        return listHerbivora
    }

    private fun showRecyclerList() {
        rv_herbivora.layoutManager = LinearLayoutManager(this)
        val listHerbivoraAdapter = ListHerbivoraAdapter(list)
        rv_herbivora.adapter = listHerbivoraAdapter
    }
}