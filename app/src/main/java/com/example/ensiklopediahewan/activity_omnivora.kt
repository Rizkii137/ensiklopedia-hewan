package com.example.ensiklopediahewan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_karnivora.*
import kotlinx.android.synthetic.main.activity_omnivora.*

class activity_omnivora : AppCompatActivity() {

    private val list = ArrayList<Omnivora>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_omnivora)

        rv_omnivora.setHasFixedSize(true)

        list.addAll(getListOmnivora())
        showRecyclerList()
    }

    fun getListOmnivora(): ArrayList<Omnivora> {
        val dataName = resources.getStringArray(R.array.data_name_omnivora)
        val dataDescription = resources.getStringArray(R.array.data_description_omnivora)
        val dataPhoto = resources.getStringArray(R.array.data_photo_omnivora)
        val listOmnivora = ArrayList<Omnivora>()
        for (position in dataName.indices) {
            val omnivora = Omnivora(
                dataName[position],
                dataDescription[position],
                dataPhoto[position]
            )
            listOmnivora.add(omnivora)
        }
        return listOmnivora
    }

    private fun showRecyclerList() {
        rv_omnivora.layoutManager = LinearLayoutManager(this)
        val listOmnivoraAdapter = ListOmnivoraAdapter(list)
        rv_omnivora.adapter = listOmnivoraAdapter
    }
}